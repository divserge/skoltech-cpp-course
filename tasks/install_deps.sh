#!/bin/sh

set -e -x

apt-get update && apt-get install -y curl

apt-get update && apt-get install -y \
    gdb \
    build-essential \
    cmake \
    ninja-build \
    clang-format \
    clang-tidy \
    g++ \
    libgtest-dev \
    sudo \
    strace

cd /usr/src/gtest
sudo cmake CMakeLists.txt
sudo make
sudo cp *.a /usr/lib/

git clone https://github.com/google/benchmark.git
cd benchmark
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=RELEASE
sudo make install