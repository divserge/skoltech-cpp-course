#include <benchmark/benchmark.h>

#include "is_prime.h"

static void BM_ISPrime_SmallTest(benchmark::State& state) {

	while (state.KeepRunning())
		benchmark::DoNotOptimize(is_prime(179426238));
}

BENCHMARK(BM_ISPrime_SmallTest);

BENCHMARK_MAIN();