#include "is_prime.h"
#include <cmath>
#include <iostream>

bool is_prime(uint32_t number) {

	for (uint32_t delim = 2; delim <= std::sqrt(number); ++delim) {
		if (number % delim == 0)
			return false;
	}

	return true;
}


