#include <gtest/gtest.h>
#include <is_prime.h>

TEST(is_prime, Simple) {
	ASSERT_TRUE(is_prime(3));
}

TEST(is_prime, NotSoSimple) {
	ASSERT_TRUE(is_prime(8));
}