project(is_prime)

cmake_minimum_required(VERSION 2.8)
include(../common.cmake)

set(SRCS is_prime.cpp)
add_library(is_prime ${SRCS})

add_gtest(test_prime test.cpp)
target_link_libraries(test_prime is_prime)

add_benchmark(bench_prime bench.cpp)
target_link_libraries(bench_prime is_prime)

